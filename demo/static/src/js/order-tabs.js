app.controller("tabCtr", ['$scope', '$rootScope', function ($scope, $rootScope) {

    $rootScope.get_product = ()=>{
        console.log('vao get product')
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        for (let i = 0; i < orders.length; i++) {
            const e = orders[i];
            if ($rootScope.selected ==  e.id) {
                $rootScope.currentCart = {
                    items: e.items
                }
            }
            
        }
    }
    const initFunc = () => {

        $scope.tabArrowDisable = true
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        // console.log('orders[0]',orders[0])
        if (!orders || orders && orders.length<1) {
            const orderId = Date.now()
            $rootScope.selected = orderId

            // $rootScope.currentCart = {
            //     id: orderId,
            //     name: 'Hóa đơn 1',
            //     idx: 1,
            //     items: [
                    // {
                    //     defaultCode: '150025000',
                    //     name: 'TV Samsung 65Q75R 65\"',
                    //     unit: 'Chiếc',
                    //     quantity: 1,
                    //     basePrice: 15000000,
                    //     total: 4 * 15000000
                    // },
                    // {
                    //     defaultCode: '1500250420',
                    //     name: 'TV Sony 65X9000G 65\"',
                    //     unit: 'Chiếc',
                    //     quantity: 1,
                    //     basePrice: 25000000,
                    //     total: 2 * 25000000
                    // },
                    // {
                    //     defaultCode: '1500255550',
                    //     name: 'TV Samsung 65Q65R 65\"',
                    //     unit: 'Chiếc',
                    //     quantity: 1,
                    //     basePrice: 12000000,
                    //     total: 1 * 12000000
                    // }
                    
            //     ],
            //     seller: {

            //     },
            //     buyer: {

            //     },
            //     totalAmount: 0,

            // }
            var orders = [{
                id: orderId,
                name: 'Hóa đơn 1',
                idx: 1,
                items: [{
                    id:02101,
                    defaultCode: '150025000',
                    name: 'TV Samsung 65Q75R 65\"',
                    unit: 'Chiếc',
                    quantity: 1,
                    basePrice: 15000000,
                    total: 4 * 15000000
                },],
                // items: prod,
                seller: {

                },
                buyer: {

                },
                totalAmount: 0,

            }]
            localStorage.setItem('pos_orders', JSON.stringify(orders))
            console.log('orders',orders)
            $rootScope.get_product()
            return $scope.orderTabs = orders
        }
        $rootScope.get_product()
        return $scope.orderTabs = orders
    }


    initFunc()
    


    // $scope.tabName = `Hóa đơn 1`;
    // $scope.tabIndex = 0;
    // $scope.tabNo = 1; 
    // $scope.menuTab =` <li class="tab-item" id="${orderId}" >
    //                     <a href="#" ng-click="selectOrder(${orderId})" > Hoa don 1 </a>
    //                 </li>
    //                 <li class="tab-move-right">
    //                 <a href="#"> > </a>
    //                 </li> 
    //                 <li class="add-tab" id="new-tab">
    //                 <a href="#" ng-click="createTab()"> + </a>
    //                 </li>`
    // $scope.orderTab = ` <li class="tab-item" id="${orderId}" >
    //                     <a href="#" ng-click="selectOrder(${orderId})" > Hoa don 1 </a>
    //                 </li>`



    $scope.createTab = () => {
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)

        var navwidth = $(".tabbar");

        let lastOrder, orderId, orderName, orderIdx
        if (orders && orders.length<1) {
            lastOrder = 0
            orderId = Date.now()
            orderName = `Hóa đơn 1`
            orderIdx = 1

        } else {
            lastOrder = orders[orders.length - 1]
            orderId = Date.now()
            orderName = `Hóa đơn ${lastOrder.idx+1}`
            orderIdx = lastOrder.idx + 1

        }


        $scope.orderTabs.push({
            id: orderId,
            idx: orderIdx,
            name: orderName
        })

        $rootScope.tabCode = orderIdx
        $scope.orderTab = `${$scope.orderTab}<li class="tab-item" id="${orderId}">
        <a href="#" ng-click="selectOrder(${orderId})">${orderName}</a>
     </li>`
        $scope.menuTab = `${$scope.orderTab} <li class="tab-move-right">
                    <a href="#"> > </a>
                    </li> 
                    <li class="add-tab" id="new-tab">
                    <a href="#" ng-click="createTab()"> + </a>
                    </li>`
        order = {
            id: orderId,
            name: orderName,
            idx: orderIdx,
            items: [],
            seller: {

            },
            buyer: {},
            totalAmount: 0,
        }

        orders.push(order)
        localStorage.setItem('pos_orders', JSON.stringify(orders))
        if (navwidth.width() >= 450) {
            $scope.tabArrowDisable = false
            let _scrollTo = navwidth.scrollLeft() + 300
            console.log(navwidth.scrollLeft(), _scrollTo)
            navwidth.scrollLeft(_scrollTo);
        }

    }

    $scope.selectOrder = (id) => {
        $rootScope.selected = id
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        orders.forEach((item) => {
            if (item.id == id) {
                console.log(item.idx)
                return $rootScope.tabCode = item.idx
            }
        })
        $rootScope.get_product()
        $rootScope.getTotal()
        console.log('$scope.selected',$scope.selected)

    }

    $scope.closeTab = (id) => {
        var navwidth = $(".tabbar").width();
        if (navwidth <= 449) {
            $scope.tabArrowDisable = true
        }

        $scope.orderTabs.forEach((item, idx) => {
            if (item.id == id) {
                return $scope.orderTabs.splice(idx, 1)
            }
        })

        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        orders.forEach((item, idx) => {
            if (item.id == id) {
                orders.splice(idx, 1)
                localStorage.setItem('pos_orders', JSON.stringify(orders))
            }
        })


    }

    $rootScope.setPrice = (prod) => {
        console.log(prod.price)
        $rootScope.discount = prod.price * 5 / 100
    }

    $rootScope.changeTotal = (prod) => {
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        console.log('prod',prod)
        prod.total = prod.quantity * prod.basePrice
        orders.forEach((e) => {
            if ($rootScope.selected == e.id) {
                console.log('e',e)
                e.items.forEach((ele) => {
                    if (ele.id == prod.id) {
                        console.log('ele',ele)
                        ele.quantity = prod.quantity
                        ele.total = prod.total
                    }
                })
            }
        })
        localStorage.setItem('pos_orders', JSON.stringify(orders))
    }

    $rootScope.changePrice = (prod)=>{
        console.log(prod)
    }

    $scope.moveToLeft = () => {
        var navwidth = $(".tabbar");
        navwidth.scrollLeft(navwidth.scrollLeft() - 300);
    }

    $scope.moveToRight = () => {
        var navwidth = $(".tabbar");
        let _scrollTo = navwidth.scrollLeft() + 300
        console.log(navwidth.scrollLeft(), _scrollTo)
        navwidth.scrollLeft(_scrollTo);
    }

    // $rootScope.sumProduct = () =>{

    // }

    $rootScope.remoProduct=(i,j)=>{
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        if($rootScope.selected){
          for (let i = 0; i < orders.length; i++) {
            const e = orders[i];
            if ($rootScope.selected ==  e.id) {
            for(var item =0;item<orders[i].items.length;item++){
                if(e.items[item].id==prod.id){
                    var index= orders[i].items.indexOf(prod)
                    orders[i].items.splice(index,1)
                }
            }
        // for(var i=0; i<$scope.lstRowProduct.length;i++){
        //   var item = $scope.lstRowProduct[i];
        //   if (item.id==product.id){
        //     var index= $scope.lstRowProduct.indexOf(product)
        //     $scope.lstRowProduct.splice(index,1)
        //    $scope.lengthLst=  $scope.lstRowProduct.length;

        //   }
        // }

        updateData();
        }
    }
    console.log('day roi',orders)
    }
    }




}])