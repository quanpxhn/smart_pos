$(function(){  

    const POD_MIN_HEIGHT = 20*$("#page-left").height()/100  
 
    $("#order-items").resizable({
        handles:'n,s'
    });
    $('#order-items').resize(function(){
        if ($('#product-items').height()<POD_MIN_HEIGHT){
            $('#product-items').height(POD_MIN_HEIGHT) 
        }
        if ($('#order-items').height()<$("#order-cart-list").height()+65){
            $('#order-items').height($("#order-cart-list").height()+65) 
        }

            $('#product-items').height($("#page-left").height()-$("#order-items").height()); 
   
    });
    $(window).resize(function(){
        if ($('#product-items').height()<POD_MIN_HEIGHT){
            $('#product-items').height(POD_MIN_HEIGHT) 
        }
        if ($('#order-items').height()<$("#order-cart-list").height()+65){
            $('#order-items').height($("#order-cart-list").height()+65) 
        }

    $('#product-items').height($("#page-left").height()-$("#order-items").height()); 
    //  $('#div1').height($("#parent").height()); 
    });

    $('[data-toggle="popover"]').popover({
        html : true,
        tempalte: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',
        content: function() {
          var content = $(this).attr("data-popover-content");
          return $(content).children(".discount-form").html();
        }
        // title: function() {
        //   var title = $(this).attr("data-popover-content");
        //   return $(title).children(".popover-heading").html();
        // }
    }); 


    $('[data-toggle="popover"]').on('show.bs.popover',function () {
        $('[data-toggle="popover"]').popover('hide');
    });    

    // $("html").on("mouseup", function (e) {
    //     var l = $(e.target);
    //     if (l[0].className.indexOf("popover") == -1) {
    //         $(".popover").each(function () {
    //             $(this).popover("hide");
    //         });
    //     }
    // });

})