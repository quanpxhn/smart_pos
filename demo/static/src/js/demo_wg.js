// odoo.define('demo.demo', function(require) {

//     var fieldRegistry = require('web.field_registry');
//     var AbstractField = require('web.AbstractField')

//     var CustomFieldChar = AbstractField.extend({
//         events:{
//             'click .o_demo':'clickDemo'
//         },
//         init: function(parent, field){
//             this._super.apply(this,arguments)
            
//         },
//         _renderReadonly: function () {
//             // implement some custom logic here\
//             this._super.apply(this,arguments)
//             console.log('hello widget');
            
//         },
//     });

//     fieldRegistry.add('demo.demo', CustomFieldChar);

//     return {CustomFieldChar:CustomFieldChar}
// });


odoo.define('demo', function(require) {
    var AbstractField = require('web.AbstractField')
    var fieldRegistry = require('web.field_registry')

    var statusField = AbstractField.extend({
        events: {
            'click .o_status': 'clickStatus'
        },
        init: function(parent, field, $node) {
            this.status = $node.data.status
            this._super.apply(this, arguments);
        },
       
        _renderReadonly: function() {
            this._super.apply(this, arguments);
            this.$el.html(() => {
                // let _inv = '000000' + this.invoice_number;
                // return '<span class="badge badge-success">' + _inv.substr(0, 7) + '</span>'
                // let _checkst = $('.o_form_sheet').find('span[name="test"]')
                console.log(this.status)
                if(this.status=="1"){
                    return '<div style="padding: 5px;border: 2px solid red;width: 5%;text-align: center; "><span style= "color:red;text-transform:uppercase">CON HANG</span></div>' 
                } else if(this.status== "2") {
                    return '<div style="padding: 5px;border: 2px solid red;width: 5%;text-align: center; "><span style= "color:red;text-transform:uppercase">HET HANG</span></div>' 
                } else{
                    return '<div style="padding: 5px;border: 2px solid red;width: 5%;text-align: center; "><span style= "color:red;text-transform:uppercase">HANG SAP VE</span></div>'
                }
            })
        },
    })

    fieldRegistry.add('demo', statusField)

    return { statusField: statusField }
});



// odoo.define('demo', function (require) {
//     'use strict';

// var widgetRegistry = require('web.widget_registry');
// var Widget = require('web.Widget');

// var RibbonWidget = Widget.extend({
//     template: 'demo',
//     xmlDependencies: ['/demo/static/src/xml/demo_wg.xml'],

//     init: function (parent, data, options) {
//         this._super.apply(this, arguments);
//         this.text = options.attrs.text;
//         this.bgColor = options.attrs.bg_color;
//     },

//     _renderTagSheet: function (node) {
//         this.has_sheet = true;
//         var $sheet = $('<div>', {class: 'clearfix o_form_sheet'});
//         var $sheet = $('<div>', {class: 'clearfix position-relative o_form_sheet'});
//         $sheet.append(node.children.map(this._renderNode.bind(this)));
//         return $sheet;
//     },

// });

// widgetRegistry.add('demo', RibbonWidget);

// return RibbonWidget;
// });


// odoo.define('demo', function (require) {
//     "use strict";
    
//     $(document).ready(function() {
    
//         $("#event_track_search").bind('keyup', function(){
//             var change_text = $(this).val();
//             $('.event_track').removeClass('invisible');
    
//             $("#search_summary").removeClass('invisible');
//             if (change_text) {
//                 $("#search_number").text($(".event_track:containsLike("+change_text+")").length);
//                 $(".event_track:not(:containsLike("+change_text+"))").addClass('invisible');
//             } else {
//                 $("#search_number").text(30);
//             }
    
//             event.preventDefault();
//         });
    
//     });
    
//     });