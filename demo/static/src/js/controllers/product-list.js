app.controller("productCtrl",  ['$scope','$rootScope','$http', function($scope,$rootScope, $http) {
    $rootScope.productList =[
        {
            id: 11,
            defaultCode: 'AD4545454',
            barCode: 'FFCD3334',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Sam lop sao vang', 
            image: 'https://fgbike.vn/wp-content/uploads/2018/10/lop-dia-hinh-xe-dap-26x1.95-08.jpg',
            unit: 'Chiec',
            onHand: 3000,
            basePrice: 30000, 

        },
        {
            id: 113,
            defaultCode: 'CCCC',
            barCode: 'FSXDDDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Nhông xích Mạnh Quang', 
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTDYi9QHYpNSZ1unMBbkqrIfvvALFuV1aA19C4RSjo1OqcYbCgu&usqp=CAU',
            unit: 'Chiec',
            onHand: 3000,
            basePrice: 5000000, 
        },
        {
            id: 02,
            defaultCode: 'A00215',
            barCode: 'HSDASJDHDF',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 4,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Xinhan Led kiểu ngắn', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/xinhan-led-kieu-ngan-1141-slide-products-5e68a467c2ea6.jpg',
            unit: 'Chiec',
            onHand: 3000,
            basePrice: 6555, 
        },
        {
            id: 03,
            defaultCode: 'A0351',
            barCode: 'FHWEFDFF',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 4,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Bao tay Daytona Arc-1 (chính hãng)', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/bao-tay-daytona-arc-1-chinh-hang-1140-slide-products-5e6220fc8bde7.jpg',
            unit: 'Chiec',
            onHand: 40,
            basePrice: 6555, 
        },
        {
            id: 04,
            defaultCode: 'B5500',
            barCode: 'UKYUGD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 4,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Kính H2C chính hãng mode chân kính', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/kinh-h2c-chinh-hang-mode-chan-kinh-1168-slide-products-5e80145397b4e.jpg',
            unit: 'Chiec',
            onHand: 40,
            basePrice: 6555, 
        },
        {
            id: 05,
            defaultCode: 'K3442',
            barCode: 'UKYUGD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 1,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Mạch led cú đêm Pro cho Vario, Click', 
            image: 'https://shop2banh.vn/images/thumbs/2020/04/mach-led-cu-dem-pro-cho-vario-click-1192-slide-products-5e86b8e5e92c4.jpg',
            unit: 'Chiec',
            onHand: 454,
            basePrice: 2344, 
        },
        {
            id: 06,
            defaultCode: 'H33444',
            barCode: 'EFWEFF',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 1,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Cần số 2 chiều cho Exciter 135', 
            image: 'https://shop2banh.vn/images/thumbs/2020/04/can-so-2-chieu-cho-exciter-135-1196-slide-products-5e8714175ad7d.jpg',
            unit: 'Chiec',
            onHand: 454,
            basePrice: 2344, 
        },
        {
            id: 07,
            defaultCode: 'U34234',
            barCode: 'GERGE',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 1,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Phuộc RCB MB2 chính hãng cho AB, PCX mẫu cũ...', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/phuoc-rcb-mb2-chinh-hang-cho-ab-pcx-mau-cu-1151-slide-products-5e76e8c579353.jpg',
            unit: 'Chiec',
            onHand: 454,
            basePrice: 2344, 
        },
        {
            id: 08,
            defaultCode: 'Y4452',
            barCode: 'WEFGD',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 5,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Heo dầu trước RCB S2 chính hãng cho Raider, Satria', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/phuoc-rcb-mb2-chinh-hang-cho-ab-pcx-mau-cu-1151-slide-products-5e76e8c579353.jpg',
            unit: 'Chiec',
            onHand: 433,
            basePrice: 2244, 
        },
        {
            id: 09,
            defaultCode: 'IFEFD',
            barCode: 'DWDWD',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 5,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Dĩa nhôm Probike 7075 A5 cho Winner 44T', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/dia-nhom-probike-7075-a5-cho-winner-44t-1146-slide-products-5e6af3e78c54e.jpg',
            unit: 'Chiec',
            onHand: 875,
            basePrice: 4453, 
        },
        {
            id: 10,
            defaultCode: 'O65635',
            barCode: '44EFWE',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 5,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Heo dầu trước RCB S2 chính hãng cho Honda Winner, Wave S', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/heo-dau-truoc-rcb-s2-chinh-hang-cho-honda-winner-wave-s-1154-slide-products-5e76ee9977788.jpg',
            unit: 'Chiec',
            onHand: 875,
            basePrice: 4453, 
        },
        {
            id: 11,
            defaultCode: 'R342342',
            barCode: 'VEFSFS',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 5,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Nhớt Repsol Matic Sintetico 4T 10W30', 
            image: 'https://shop2banh.vn/images/thumbs/2019/06/nhot-repsol-matic-sintetico-4t-10w30-969-slide-products-5d0472c346360.jpg',
            unit: 'Chiec',
            onHand: 875,
            basePrice: 4453, 
        },
        {
            id: 12,
            defaultCode: 'G3423',
            barCode: 'EFVFSS',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 5,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Nhớt Fuchs Silkolene Pro 4 10W40 XP (100ml)', 
            image: 'https://shop2banh.vn/images/thumbs/2020/02/nhot-fuchs-silkolene-pro-4-10w40-xp-100ml-1112-slide-products-5e3d31f32fc9e.jpg',
            unit: 'Chiec',
            onHand: 731,
            basePrice: 3322, 
        },
        {
            id: 13,
            defaultCode: 'E33423',
            barCode: 'HTHRT',
            qrCode: 'https://caigicungco',
            productType: 2,
            catetgoryId: 5,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Tay thắng chính hãng cho Winner, Wave, Future', 
            image: 'https://shop2banh.vn/images/thumbs/2020/04/tay-thang-chinh-hang-cho-winner-wave-future-1182-slide-products-5e869b34117e8.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 14,
            defaultCode: 'I7886',
            barCode: 'GERGERG',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Vỏ Michelin 140/70-17 Pilot Street 2', 
            image: 'https://shop2banh.vn/images/thumbs/2020/04/vo-michelin-14070-17-pilot-street-2-1189-slide-products-5e845d339036a.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 15,
            defaultCode: 'H3444',
            barCode: 'EKVNDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Niềng nhôm RCB 1.6 chính hãng', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/nieng-nhom-rcb-16-chinh-hang-1177-slide-products-5e81adcf6499b.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 16,
            defaultCode: 'H3444',
            barCode: 'EKVNDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Hộp đựng đồ AB 2020', 
            image: 'https://shop2banh.vn/images/thumbs/2020/04/hop-dung-do-ab-2020-1195-slide-products-5e8711d3e0f0d.jpeg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 17,
            defaultCode: 'H3444',
            barCode: 'EKVNDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Nón bảo hiểm KYT Venom 3/4 chính hãng (trắng/đen trơn)', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/non-bao-hiem-kyt-venom-34-chinh-hang-trangden-tron-1164-slide-products-5e800c80d29e5.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 18,
            defaultCode: 'H3444',
            barCode: 'EKVNDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Baga inox 10ly sơn đen tĩnh điện cho AB', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/baga-inox-10ly-son-den-tinh-dien-cho-ab-1169-slide-products-5e8017e21a2b6.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 19,
            defaultCode: 'H3444',
            barCode: 'EKVNDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Vỏ Michelin 60/90-17 Pilot Street 2', 
            image: 'https://shop2banh.vn/images/thumbs/2020/04/vo-michelin-6090-17-pilot-street-2-1187-slide-products-5e845abdf41d9.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },
        {
            id: 20,
            defaultCode: 'H3444',
            barCode: 'EKVNDD',
            qrCode: 'https://caigicungco',
            productType: 3,
            catetgoryId: 3,
            isActive: true,
            hasVariants: false,
            variantCount: 0,
            allowsSale: true, 
            name: 'Dĩa nhôm Probike 7075 A3 cho Winner 44T', 
            image: 'https://shop2banh.vn/images/thumbs/2020/03/dia-nhom-probike-7075-a3-cho-winner-44t-1149-slide-products-5e6af8c18860f.jpg',
            unit: 'Chiec',
            onHand: 865,
            basePrice: 43343, 
        },

    ]

    $scope.page = 0;
    // $scope.items = [ "a", "b", "c", "d", "e", "f", "g" ];
    $scope.itemsLimit = 6;
    $scope.totalPage = Math.ceil($rootScope.productList.length/$scope.itemsLimit)

    $scope.itemsPaginated = function () {
    const height =   $('#product-items').height()
    const row = Math.floor((height-40)/132)
    
    $scope.itemsLimit = 6*row
    $scope.totalPage = Math.ceil($rootScope.productList.length/$scope.itemsLimit)
    var currentPageIndex = $scope.page * $scope.itemsLimit;
    return $rootScope.productList.slice(
        currentPageIndex, 
        currentPageIndex + $scope.itemsLimit);
    };


    // $scope.page = 1 

    // $rootScope.prodWrapperHeigth

    // $rootScope.getProducts = async () =>{ 
    //     if ($scope.page<1) { $scope.page=1}
    //     // Check 
    //     const height =   $('#product-items').height()
    //     const row = Math.floor(height/132)
    //     const data = [...$rootScope.productList]
    //     const page =   await chunkArray(data, 6*row)
       
    //     if (page[$scope.page-1]) { 
    //         return  page[$scope.page-1] 
    //     }
    //     return;
        
    // }


    $scope.selectProduct = (prod)=>{
        var orders = localStorage.getItem('pos_orders')
        orders = JSON.parse(orders)
        for (let k = 0; k < orders.length; k++) {
            const ele = orders[k];
            if (ele.id == $rootScope.selected) {
                if(ele.items.length == 0){
                    prod.quantity = 1
                    prod.total = prod.quantity * prod.basePrice
                    ele.items.push(prod)
                }
                else{
                    var loop = false
                    for (let i = 0; i < ele.items.length; i++) {
                        const e = ele.items[i];
                        if(prod.id == e.id){
                            e.quantity += 1
                            e.total = e.quantity * e.basePrice
                            loop = true
                        }
                    }
                    if(!loop){
                        prod.quantity = 1
                        prod.total = prod.quantity * prod.basePrice
                        ele.items.push(prod)
                    }
                }
                localStorage.setItem('pos_orders', JSON.stringify(orders))
                $rootScope.get_product()
                console.log('vao day',orders)
            }
        }
        // console.log('$rootScope.currentCart',$rootScope.currentCart)
        // push_orders(prod)
        // $rootScope.currentCart.push(angular.copy(prod))
        // console.log(tabCtr.get())
    }

    $scope.setProducts = async ()=>{
        
        $scope.prods =  await $rootScope.getProducts()

        console.log($scope.prods, '/',  $rootScope.productList)
        return;
    }

    // $scope.setProducts()


    //  Common function for controller here
    const chunkArray = (myArray, chunk_size) =>{
        var results = [];
        
        while (myArray.length) {
            results.push(myArray.splice(0, chunk_size));
        } 
        return results;
    }

   


    $http.get('https://jsonplaceholder.typicode.com/todos/1', null).then(res=>{
        $scope.apiT = res 
    }, err=>{
        $scope.apiT =err
    });




}])
