
app.controller('orderInfoCtrl',['$scope','$rootScope','$http', '$interval',  function($scope,$rootScope, $http, $interval) {
    var tick =  ()=> {
      $scope.clock = Date.now();
    }
    tick();
    $interval(tick, 10000);

    $scope.discountValues=0
    $rootScope.getTotal = function(){
      var total = 0;
      var orders = localStorage.getItem('pos_orders')
      orders = JSON.parse(orders)
      if($rootScope.selected){
        console.log('tab chon :', $rootScope.selected)
        for (let i = 0; i < orders.length; i++) {
          const e = orders[i];
          if ($rootScope.selected ==  e.id) {
            e.items.forEach((ele) => {
              total += ele.total;
            });
            return total;
          }
        }
      }
    }

    $rootScope.getLastTotal = function(){
      var custom_pay= $rootScope.getTotal()-$scope.discountValues;
      return custom_pay;
    }

    $rootScope.totalAmount = function(){
      var quantity = 0;
      var orders = localStorage.getItem('pos_orders')
      orders = JSON.parse(orders)
      console.log(orders)
      if($rootScope.selected){
        for(let i = 0; i < orders.length; i ++){
          const e = orders[i];
          if($rootScope.selected == e.id){
            e.items.forEach((ele)=>{
              quantity += ele.quantity;
            });
            return quantity;
          }
        }
      }
    }

  }]);