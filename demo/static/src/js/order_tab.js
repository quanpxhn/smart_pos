;
// Create an Application named "myApp".
var app = angular.module("myApp", ['ngSanitize']);
angular.module('myApp')
.directive('compile', ['$compile', function ($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compile);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
   )};
}]).controller("myCtrl",  ['$scope','$rootScope', '$timeout','$http',function($scope,$rootScope, $timeout,$http) {
    
    $scope.loaded = false;
    $scope.operator = "+";
    $rootScope.tabCode = 0;
    $scope.variable1 = 30;
    $scope.variable2 = 20;
    $scope.result = 0;
    
    $timeout(function() { $scope.loaded = true; }, 1000);
    
    // $http({
    //     method: 'POST',
    //     url: 'http://localhost:8069/api/customer-list',
    //   }).then(function successCallback(response) {
    //       console.log(response)
    //     }, function errorCallback(response) {
    //       // called asynchronously if an error occurs
    //       // or server returns response with an error status.
    //     });

    $scope.setOperatorSum = function() {
        $scope.operator = "+";
    }
 
    $scope.setOperatorMinus = function() {
        $scope.operator = "-";
    }
 
    $scope.calculate = function() {
        if ($scope.operator == "+") {
            $scope.result = parseFloat($scope.variable1) + parseFloat($scope.variable2);
        } else if ($scope.operator == "-") {
            $scope.result = parseFloat($scope.variable1) - parseFloat($scope.variable2);
        }
    }

    var a = 1
    $scope.createNewTab = ()=>{
        $scope.tabCode = a++;
    }
 
}]);

$(function(){  

    // $('.tab-item').on('click', function(){
    //     $('.tab-item').removeClass('selected');
    //     $(this).addClass('selected');
    // });


    // console.log('Chieu tao tong ',$("#page-left").height())
    // $("#product-items").resizable();
    // $('#product-items').resize(function(){
    // $('#order-items').height($("#page-left").height()-$("#product-items").height()); 
    // console.log('Chieu tao tong ',$("#page-left").height())
    // });
    // $(window).resize(function(){
    // $('#order-items').height($("#page-left").height()-$("#product-items").height()); 
    // //  $('#div1').height($("#parent").height()); 
    // });

    // const init = ()=>{
        


    //     $('#tab-1').attr('id',firstOrderTabId)
    

    // }

    // init()
   
    // $('#new-tab').click(()=>{
    //     var orders = localStorage.getItem('pos_orders')
    //     orders = JSON.parse(orders)
       
    //     lastOrder = orders[orders.length-1] 
    //     orderId = Date.now()
    //     orderName = `Hóa đơn ${lastOrder.idx+1}`
    //     orderIdx = lastOrder.idx+1
    //     $('.tabbar .tab-move-right').before(`<li class="tab-item" id="${orderId}" >
    //     <a href="#">  ${orderName} </a>
    //  </li>`)
    //             order = {
    //                 id: orderId,
    //                 name: orderName,
    //                 idx: orderIdx,
    //                 items:[],
    //                 seller: {

    //                 },
    //                 buyer:{ 
    //                 },
    //                 totalAmount: 0, 
    //             }
    //     console.log(orders.length)
    //     orders.push(order)
    //     localStorage.setItem('pos_orders', JSON.stringify(orders))
    // })
})