# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json

class SmartPos(http.Controller):
    @http.route('/shop', auth="user")
    def index(self, **kw):
        View = request.env['ir.ui.view'].sudo()
        content =    View.render_template('demo.shop_layout',{'contents':234})
        return content

#     @http.route('/smart_pos/smart_pos/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('smart_pos.listing', {
#             'root': '/smart_pos/smart_pos',
#             'objects': http.request.env['smart_pos.smart_pos'].search([]),
#         })

#     @http.route('/smart_pos/smart_pos/objects/<model("smart_pos.smart_pos"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('smart_pos.object', {
#             'object': obj
#         })


    @http.route('/list',  type='http', auth='public', methods=['GET'])
    def list(self, **kw):
        lst_product=[]
        http.request.env.cr.execute(
            """
            select pt.id,pt.name,pp.default_code,
            sq.quantity-sq.reserved_quantity inventory, sq.reserved_quantity purchase ,
            pt.list_price 
            from product_template  pt left join product_product pp on pp.product_tmpl_id =pt.id 
            left join stock_quant sq on pp.id= sq.product_id
            left join stock_location sl 
            on sl.id= sq.location_id
            where sq.quantity-sq.reserved_quantity >=0 
            """
        )
        # obj=http.request.env['product.category'].browse(2)
        # obj.write({'name':'update'})
        # http.request.env['product.category'].create({
        #     'name':'create',
        #     'complete_name':'create',
        #     'create_uid':1,
        # })
        # http.request.env['product.category'].search([('id','=','5')]).unlink()
       
        self.data= http.request.env.cr.fetchall()
        for item in self.data:
            vals={
                'id':item[0],
                'name':item[1],
                'code' :item[2],
                'quantity':1,
                'inventory':item[3],
                'purchase' :item[4],
                'price':item[5]

            }
            lst_product.append(vals)
        return json.dumps(lst_product)  
    
    @http.route('/api/customer-list', type='http',auth='public',csrf=False,cors=None)
    def get_customer(self,**kw):
        customer_list = []
        response = request.env['res.partner'].search([])
        for res in response:
            customer_list.append(res.name)
        return json.dumps(customer_list)