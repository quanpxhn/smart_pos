# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import timedelta

class CustemDemoDasboard(models.Model):
    _name = 'custom.demo.dashboard'

    color = fields.Integer(string='Color Index')
    name =fields.Char(string='Name')


class book(models.Model):
    _name = 'books.books'

    name_book = fields.Char(string="Name")
    type_book = fields.Selection(string="Trạng Thái",
        selection=[('0',"Ngôn Tình"),('1',"Trinh Thám"),('2',"Kinh Dị")],store=True)
    muon_sach = fields.Integer(string="Thue Sach",compute="_get_borrower_count",store=True)
    book_pn_ids = fields.Many2many('res.partner', string="Partner Retal")
    # book_id = fields.Many2many(comodel='demo.demo',string='ID')
    books_rt = fields.Many2many("demo.demo",inverse_name='id',string="Book",required=True)

    #graph
    @api.depends('book_pn_ids')
    def _get_borrower_count(self):
        for r in self:
            r.muon_sach=len(r.book_pn_ids)


class demo(models.Model):
    _name = 'demo.demo'

    book_ids = fields.Many2many('res.partner', string="Book RT")
    name_book_retal = fields.Char()
    borrow_date = fields.Date(string="Brrow Date",default=fields.Date.today)
    pay_date = fields.Date(string="Pay Date",store=True,compute='_get_end_date',inverse='_set_end_date')
    duration = fields.Float()
    borrower_count = fields.Integer(string="Borrower Count",compute="_get_borrower_count",store=True)
    user_signature = fields.Binary(string="Singture")
    name_author = fields.Many2many('ir.attachment',string='Author')
    status = fields.Selection(string='Status',selection=[('0','Hang Sap Ve'),
                                                          ('1','Con Hang'),  
                                                          ('2','Het Hang'),])
    test = fields.Selection(string='Ribbon',selection=[('0','Hang Sap Ve'),
                                                          ('1','Con Hang'),  
                                                          ('2','Het Hang'),])


    #calender
    @api.depends('borrow_date','duration')
    def _get_end_date(self):
        for r in self:
            if not (r.borrow_date and r.duration):
                r.pay_date = r.borrow_date
                continue
            duration = timedelta(days=r.duration,seconds=-1)
            r.pay_date = r.borrow_date + duration
    def _set_end_date(self):
        for r in self:
            if not (r.borrow_date and r.pay_date):
                continue
            r.duration = (r.pay_date - r.borrow_date).days + 1
    
    #graph
    @api.depends('book_ids')
    def _get_borrower_count(self):
        for r in self:
            r.borrower_count=len(r.book_ids)