from odoo import api,models,fields

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    sale_tax_id = fields.Many2one('account.tax', string="Default Sale Tax", readonly=False)
    retal_can_purchase = fields.Boolean(string="Book can purchase", help="The transactions are processed by Vantiv. Set your Vantiv credentials on the related payment journal.")
    retal_can_sale = fields.Boolean("Multiple Product Prices", config_parameter='point_of_sale.pos_sales_price')
    retal_pricelist = fields.Selection([
        ('percentage', 'Multiple prices per product (e.g. customer segments, currencies)'),
        ('formula', 'Price computed from formulas (discounts, margins, roundings)')
        ], string="POS Pricelists", config_parameter='point_of_sale.pos_pricelist_setting')